import java.util.Scanner;

class Section3_4 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number : ");
		int a = sc.nextInt();

		if(a >= 0) System.out.print("Number is Positive");
		else System.out.print("Number is negative");
	}
}