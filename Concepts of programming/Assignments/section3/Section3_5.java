import java.util.Scanner;

class Section3_5 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a Number : ");
		int a = sc.nextInt();

		if(a%2 == 0) System.out.print("Number is Even");
		else System.out.print("Number is Odd");
	}
}