import java.util.Scanner;

class Section3_12 {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Temperature in degree centigrate: ");		
		double temp = sc.nextDouble();

		if(temp < 0) System.out.print("Freezing weather");
		else if(temp>=0 && temp <10) System.out.print("Very Cold weather");
		else if(temp>=10 && temp<20) System.out.print("Cold weather");
		else if(temp>=20 && temp<30) System.out.print("Normal in Temp");
		else if(temp>=30 && temp <40) System.out.print("Its Hot");
		else System.out.print("Its Very Hot");
	}
}