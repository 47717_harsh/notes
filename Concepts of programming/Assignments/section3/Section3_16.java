import java.util.Scanner;

class Section3_16 {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Marks of physics chemistry and math : ");		
		int phy = sc.nextInt();
		int chem = sc.nextInt();
		int math = sc.nextInt();
		double avg = (phy+chem+math)/3.0;

		if(avg < 30) System.out.print("You are failed");
		else if(avg >= 30 && avg < 60) System.out.print("You passed with Second division");
		else if(avg >= 60 && avg < 80) System.out.print("You passed with First division");
		else System.out.print("You passed with First class distinction");
	}
}