import java.util.Scanner;

class Section3_3 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Two Numbers : ");
		int a = sc.nextInt();
		int b = sc.nextInt();

		if(a == b) System.out.print("Both are equal");
		else System.out.print("Both are not equal");
	}
}