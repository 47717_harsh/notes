import java.util.Scanner;

class Section3_18 {
	
	public static void main(String[] args) {
	
	Scanner sc = new Scanner(System.in);
	System.out.print("Enter a character : ");
	char ch = sc.next().charAt(0);

	if(ch == 'a' || ch == 'A' || ch == 'e' || ch == 'E' || ch == 'i' || ch == 'I' || ch == 'o' || ch =='O' || ch =='u' || ch == 'U')
		System.out.print("Vovel");
	else if( ((int)ch >= 65 && (int)ch <=90) || ((int)ch >=97 && (int)ch <=122) )
		System.out.print("Consonant");
	else
		System.out.print("Other Character");

	}
}