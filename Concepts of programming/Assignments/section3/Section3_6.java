import java.util.Scanner;

class Section3_6 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Age : ");
		int a = sc.nextInt();

		if(a >= 18) System.out.print("You are eligible to cast vote");
		else System.out.print("You are not eligible to cast vote");
	}
}