import java.util.Scanner;

class Section4_17 {
	
	static int factorial(int num)
	{
		int fact = 1;

		for(int i=1;i<=num;i++)
		{
			fact = fact*i;
		}
		return fact;
	}

	static int sumOfFact(int num)
	{
		int sum = 0;

		while(num != 0)
		{
			sum = sum + factorial(num%10);
			num = num/10;
		}
		return sum;
	}
	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		
		if(num == 0) System.out.print("Not a Strong Number");
		else if (num == sumOfFact(num))
			System.out.print("Strong Number");
		else
			System.out.print("Not a Strong Number");

	}
}