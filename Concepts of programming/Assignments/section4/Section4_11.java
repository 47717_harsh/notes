import java.util.Scanner;

class Section4_11 {

	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int sum = 0;

		for(int i=1;i<=num/2;i++)
		{
			if(num%i == 0)
				sum = sum + i;
		}
		if(num == sum)
			System.out.println("Perfect Number");
		else 
			System.out.println("Not a Perfect Number");
	}
}