import java.util.Scanner;

class Section4_13 {
	

	static boolean isPrime(int num)
	{
		if(num == 0) return false;
		
		for(int i=2;i<=num/2;i++)
		{
			if(num%i == 0) return false;
		}

		return true;
	}
	public static void main(String[] args) {
		
		System.out.print("Enter a Number : ");

		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		if(isPrime(num))
			System.out.print("Prime Number");
		else
			System.out.print("Not a Prime Number");

	}
}