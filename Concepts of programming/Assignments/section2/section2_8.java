import java.util.Scanner;

class section2_8 {
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter name : ");
		String name = sc.nextLine();

		System.out.print("Enter Roll no. : ");
		int rollNo = sc.nextInt();
		
		System.out.print("Enter Marks of Physics, Chemistary and Computer : ");
		double phy = sc.nextDouble();
		double che = sc.nextDouble();
		double com = sc.nextDouble();


		System.out.println("Roll No :" + rollNo);
		System.out.println("Name :" + name);
		System.out.println("Marks in Physics :" + phy);
		System.out.println("Marks in Chemistry :" + che);
		System.out.println("Marks in Computer :" + com);
		System.out.println("Total Marks :" + (phy+che+com));
		System.out.println("Percentage :" + (phy+che+com)/3 );		

	}
}