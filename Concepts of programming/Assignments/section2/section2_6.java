import java.util.Scanner;

class section2_6 {
	
	public static void main(String[] args) {
	
		System.out.print("Enter Radius of circle : ");

		Scanner sc = new Scanner(System.in);

		double radius = sc.nextDouble();
		double area = Math.PI * radius * radius;
		double peri = 2 * Math.PI * radius;

		System.out.format("Area      : %.3f\n", area);
		System.out.format("Perimeter : %.3f", peri);		

	}
}