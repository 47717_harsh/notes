#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
//Details - man 2 fork
int main(int argc, char *argv[])
{
    pid_t ret_val;
    ret_val = fork();
    /*
    ret_val == 0 - child process is created {child process block}
    ret_val > 0 - parent process {parent process block}
    ret_val < 0 - error in executing the fork system call

    Note : 
    Order of the parent and child is not fixed;
    It is decided by OS.
    There could be a case that some iterations are executed of parent and then child and vice-versa.
    */
       
   if (ret_val == 0)
   {
        for(int index = 0; index <30; index++)
        printf("I am child process and my index value is : %d\n",index);
   }
   else if (ret_val > 0)
   {
       for(int index = 0; index <30; index++)
        printf("I am Parent process and my index value is : %d\n",index);
   }
   else
   {
       printf("failed to create child process \n");
       exit(0);
       
   }
    printf("Statement Outisde the loop\n");
    return 0;
}