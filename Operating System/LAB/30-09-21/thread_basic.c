//link with -lpthread
#include <stdio.h>
#include <pthread.h>
/*
Thread routine - argument must be supplied with void *
Routine - I
*/
void *msg(void *ptr)
{
    printf("Today is Thursday\n");
    printf("Routine for thread 1\n");
}
/*
Thread routine - II
*/

void *quote_Of_The_Day(void *ptr)
{
    printf("Success is not an event; it will happen gradually\n");
    printf("Thread -->2\n");
}
void main()
{
        pthread_t thread1, thread2;
        printf("Before Thread\n");
        int ret1 = pthread_create(&thread1,NULL,msg,NULL);
        int ret2 = pthread_create(&thread2,NULL,quote_Of_The_Day,NULL);
	/*
	pthread_join will block the calling thread i.e. main thread
	to complete the execution of thread which is passed as a first arugument
	Note:
	if pthread_join will not be used : It may happen that main thread has complete its execution and some statement doesn't
	yield the results e.g. printf takes time to send buffer data to console
	*/
        pthread_join(thread1,NULL);
        pthread_join(thread2, NULL);
       
        //return 0;

}