#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
//Details - man 2 fork
int main(int argc, char *argv[])
{
    fork();
    fork();
    while(1)
    {
     printf("Statement written after fork system call\n");
     sleep(5);
    }
}
/*

pid of the parent process is ppid of the child process.
ps -eo user,pid,ppid,cmd | grep fork_demo.out
bhupend+  936841  779436 ./fork_demo.out
bhupend+  936842  936841 ./fork_demo.out
bhupend+  942655  746136 grep --color=auto fork_demo.out
--------------------------------------------------------------
iot$ ps -eo user,pid,ppid,cmd | grep fork_demo.out
bhupend+  971775  779436 ./fork_demo.out
bhupend+  971776  971775 ./fork_demo.out
bhupend+  971777  971775 ./fork_demo.out
bhupend+  971778  971776 ./fork_demo.out
bhupend+  971842  746136 grep --color=auto fork_demo.out
*/