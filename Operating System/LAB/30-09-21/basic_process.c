#include <stdio.h>
#include <unistd.h>
int main(int argc, char* argv[])
{
    while(1)
    {
        printf("A simple program : About to load to memory to become process \n");
        sleep(5);  //delay in seconds
    }
    return 0;
}
/*
    Steps to compile: 
    gcc basic_process.c -o basic_process.out
    ./basic_process.out
    ------------------------------------------------------------
    List Entry of the process : 
    ps -eo user,pid,ppid,cmd,stat | grep basic_process.out
*/

